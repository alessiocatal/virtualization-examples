#!/usr/bin/env python
# encoding: utf-8
import json
from flask import Flask, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return jsonify({'name': 'Mario Rossi',
                       'email': 'mariorossi@outlook.com'})

if __name__ == "__main__":
    app.run(debug=True, port=5001, host="0.0.0.0")