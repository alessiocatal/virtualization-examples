#!/usr/bin/env python
# encoding: utf-8
import json
from flask import Flask, jsonify
from flask_cors import CORS
from pymongo import MongoClient

client = MongoClient("mongodb")
db = client['test']

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return jsonify({'name': 'Mario Rossi',
                       'email': 'mariorossi@outlook.com'})

@app.route('/insertData')
def insertData():
    try:
        db.myColl.insert_one({'name': 'Mario Rossi',
                           'email': 'mariorossi@outlook.com'})
        return "Data Inserted"
    except:
        return "Error in Insert Phase"

@app.route('/removeData')
def deleteData():
    try:
        db.myColl.delete_many({})
        return "Data Deleted"
    except:
        return "Error in Insert Phase"


@app.route('/getData')
def getDataFromDB():
    result = db.myColl.find({},{"_id":0})
    arr = []
    for element in result:
        print(element)
        arr.append(element)

    return jsonify(arr)


if __name__ == "__main__":
    app.run(debug=True, port=5001, host="0.0.0.0")